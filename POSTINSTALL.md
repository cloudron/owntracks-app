This app is pre-setup with one user account.
The initial credentials are:

**Username**: cloudron<br/>
**Password**: changeme123<br/>

Please change the user password immediately as follows:

Mosquitto user account in `/app/data/mosquitto/passwd`:

```
mosquitto_passwd -b /app/data/mosquitto/passwd cloudron NEW-PASSWORD
```

OwnTracks user password in `/app/data/nginx/owntracks.htpasswd`:

```
htpasswd -b /app/data/nginx/owntracks.htpasswd cloudron NEW-PASSWORD
```

If you are creating a new user or renaming an existent user, you should update `OTR_TOPICS` variable in `/app/data/env`, e.g.

```
OTR_TOPICS="owntracks/# owntracks/cloudron/# owntracks/NEW-USER/#"
```

Also you need to add/ammend User specific topic ACLs in `/app/data/mosquitto/mosquitto.acl`, e.g.

```
user cloudron
topic readwrite owntracks/cloudron/#
```

Don't forget to restart the app to apply the changes.

Please refer to `mosquitto` documentation for detail at https://mosquitto.org/man/mosquitto-conf-5.html
