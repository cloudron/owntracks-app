FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4 as frontend-builder

ARG VERSION=2.12.0

RUN mkdir -p /tmp/frontend

WORKDIR /tmp/frontend

RUN curl -L https://github.com/owntracks/frontend/archive/refs/tags/v${VERSION}.tar.gz | tar zxf - -C /tmp/frontend --strip-components 1
RUN yarn install

ENV NODE_ENV="production"
RUN yarn build

FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4 as recorder-builder

ARG VERSION=0.9.7

ENV DOCKER_RUNNING=1

RUN apt-get update && \
    apt-get install -y \
        coreutils \
        liblmdb-dev \
        musl-dev \
        libcurl4-openssl-dev \
        libconfig-dev \
        libmosquitto-dev \
        mosquitto-dev \
        libsodium-dev \
        lua5.2-dev \
        util-linux

RUN mkdir -p /tmp/recorder

WORKDIR /tmp/recorder

RUN curl -L https://github.com/owntracks/recorder/archive/refs/tags/${VERSION}.tar.gz | tar zxf - -C /tmp/recorder --strip-components 1
RUN wget -O /tmp/recorder/config.mk https://github.com/owntracks/docker-recorder/raw/846dea2fea24e1d1c913031d1a49bf547b3d229e/config.mk
RUN make -j $(nprocs)
RUN make install DESTDIR=/app/code/recorder

FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4 as runner

RUN mkdir -p /app/pkg/recorder

RUN apt-get update && \
    apt-get install -y \
        mosquitto \
        mosquitto-clients \
	liblmdb0 \
        lmdb-utils \
	libsodium23 \
	lua5.2 \
	util-linux \
	tzdata \
        libconfig9 \
        liblua5.2-0

WORKDIR /app/code

ENV DOCKER_RUNNING=1

ENV TZ="UTC"

RUN wget -O /app/pkg/recorder/recorder.conf https://github.com/owntracks/docker-recorder/raw/846dea2fea24e1d1c913031d1a49bf547b3d229e/recorder.conf
RUN wget -O /app/pkg/recorder/JSON.lua https://github.com/owntracks/docker-recorder/raw/846dea2fea24e1d1c913031d1a49bf547b3d229e/JSON.lua
# https://github.com/owntracks/docker-recorder/tree/0a779838e810fac74f7d64084768ab8b21ab3e7d?tab=readme-ov-file
# In order to make ot-recorder talk SSL & accept Let's encrypt certificates a kind of concatenated le-ca.pem is needed.
# https://gist.github.com/jpmens/211dbe7904a0efd40e2e590066582ae5
RUN wget -O /app/pkg/recorder/ca.pem https://gist.github.com/jpmens/211dbe7904a0efd40e2e590066582ae5/raw/507af88aed770889ba996b8f84c82c7bbab03295/le-all.pem

COPY --from=recorder-builder /app/code/recorder /app/code/recorder
RUN mv /app/code/recorder/usr/bin /app/code/recorder/ && \
    mv /app/code/recorder/usr/sbin/* /app/code/recorder/bin/ && \
    rm -rf /app/code/recorder/usr
ENV PATH=$PATH:/app/code/recorder/bin/

RUN chmod +r /app/code/recorder/config/recorder.conf && \
    chmod 444 /app/code/recorder/config/timezone16.bin && \
    ln -sf /run/recorder/config /config && \
    ln -sf /app/code/recorder/htdocs /htdocs && \
    ln -sf /app/data/store /store

# frontend
ENV NODE_ENV="production"
COPY --from=frontend-builder /tmp/frontend/dist /app/code/html
RUN ln -sf /run/frontend/config.js /app/code/html/config/config.js

# configure nginx
RUN rm /etc/nginx/sites-enabled/* && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    ln -sf /run/owntracks/owntracks-nginx.conf /etc/nginx/sites-enabled/owntracks.conf
COPY nginx/owntracks-nginx.conf /app/pkg/owntracks-nginx.conf
COPY nginx/readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf

# Add supervisor configs
COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/owntracks/supervisord.log /var/log/supervisor/supervisord.log

COPY start.sh /app/pkg/

RUN chmod +x /app/pkg/start.sh

CMD [ "/app/pkg/start.sh" ]
