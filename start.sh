#!/bin/bash

set -eu

mkdir -p /run/owntracks \
         /run/recorder/config \
         /run/frontend \
         /run/mosquitto \
         /app/data/mosquitto \
         /app/data/nginx

export OTR_STORAGEDIR=/app/data/store
export OCAT_STORAGEDIR=/app/data/store

if [[ ! -f /app/data/env ]]; then
    OTR_USER=recorder
    OTR_PASS=$(openssl rand -hex 16)
    cat > /app/data/env <<EOT
# TZ=Europe/Berlin
TZ=UTC
OTR_SERVERLABEL="OwnTracks"
OTR_TOPICS="owntracks/# owntracks/cloudron/#"
OTR_USER="${OTR_USER}"
OTR_PASS="${OTR_PASS}"
EOT
fi
grep -v '^#' /app/data/env \
    | xargs -0 printf \
    | while read line; do echo "export "${line}; done \
    > /run/owntracks/env.sh
source /run/owntracks/env.sh

echo "==> Updating Recorder configs"
cp -R /app/code/recorder/config/. /run/recorder/config
cp /app/pkg/recorder/JSON.lua /run/recorder/config/
cp /app/pkg/recorder/ca.pem /run/recorder/config/
# default config
# cp /app/pkg/recorder/recorder.conf /run/recorder/config/

if [[ ! -f /run/frontend/config.js ]]; then
    echo "==> Updating Frontend config"
#    cp /app/code/html/config/config.example.js /app/data/frontend/config.js
    cat > /run/frontend/config.js <<EOT
window.owntracks = window.owntracks || {};

window.owntracks.config = {
  api: {
    baseUrl: "${CLOUDRON_APP_ORIGIN}/owntracks/",
  },
};
EOT
fi

cat > /run/recorder/config/recorder.conf <<EOT
OTR_TOPICS = "${OTR_TOPICS}"
OTR_STORAGEDIR = "${OTR_STORAGEDIR}"
OTR_SERVERLABEL = "${OTR_SERVERLABEL}"

# Address or hostname of the MQTT broker
OTR_HOST = "localhost"
#OTR_PORT = ${MQTT_PORT}
#OTR_USER = "${OTR_USER}"
#OTR_PASS = "${OTR_PASS}"
#OTR_CAFILE = "/config/ca.pem"
# OTR_QOS = 2
# OTR_CLIENTID = ""
#OTR_CAFILE = "ISRG_Root_X1.pem"
#OTR_CAPATH = "/etc/ssl/certs"
#OTR_CERTFILE = "/etc/certs/tls_cert.pem"
#OTR_KEYFILE = "/etc/certs/tls_key.pem"
#OTR_IDENTITY = ""
#OTR_PSK = ""
#OTR_GEOKEY = ""
#OTR_PRECISION = 7

OTR_HTTPHOST = "localhost"
OTR_HTTPPORT = 8088
# OTR_HTTPLOGDIR = ""
#OTR_BROWSERAPIKEY = ""
OTR_VIEWSDIR = "/app/code/recorder/htdocs/views"
#OTR_HTTPPREFIX = ""
#OTR_LUASCRIPT = ""
EOT

if [[ ! -f ${OTR_STORAGEDIR}/ghash/data.mdb ]]; then
    mkdir -p ${OTR_STORAGEDIR}/last ${OTR_STORAGEDIR}/rec
    ot-recorder --initialize
fi

if [[ ! -f /app/data/mosquitto/passwd ]]; then
    mosquitto_passwd -c -b /app/data/mosquitto/passwd "${OTR_USER}" "${OTR_PASS}"
    mosquitto_passwd -b /app/data/mosquitto/passwd cloudron changeme123
fi

if [[ ! -f /app/data/mosquitto/mosquitto.acl ]]; then
    cat > /app/data/mosquitto/mosquitto.acl <<EOT
user cloudron
topic readwrite owntracks/cloudron/#
EOT
fi

cat > /run/mosquitto/mosquitto.acl <<EOT
# This affects access control for clients with no username.
topic read \$SYS/#

user recorder
topic read owntracks/#
EOT

cat /app/data/mosquitto/mosquitto.acl >> /run/mosquitto/mosquitto.acl

cat >> /run/mosquitto/mosquitto.acl <<EOT
# This affects all clients.
pattern write \$SYS/broker/connection/%c/state
EOT



if [[ ! -f /run/mosquitto/mosquitto.conf ]]; then
    cat > /run/mosquitto/mosquitto.conf <<EOT
allow_anonymous false
acl_file /run/mosquitto/mosquitto.acl
password_file /app/data/mosquitto/passwd

listener 1883
socket_domain ipv4

listener ${MQTT_PORT}
certfile /etc/certs/tls_cert.pem
cafile /config/ca.pem
keyfile /etc/certs/tls_key.pem

listener ${MQTT_WEBSOCKETS_PORT}
protocol websockets
certfile /etc/certs/tls_cert.pem
cafile /config/ca.pem
keyfile /etc/certs/tls_key.pem
EOT
fi

echo "==> Updating nginx config"
cp /app/pkg/owntracks-nginx.conf /run/owntracks/owntracks-nginx.conf
if [[ ! -f /app/data/nginx/owntracks.htpasswd ]]; then
    htpasswd -bc /app/data/nginx/owntracks.htpasswd "${OTR_USER}" "${OTR_PASS}"
    htpasswd -b /app/data/nginx/owntracks.htpasswd cloudron changeme123
fi

echo "==> Ensure permissions"
chown -R cloudron:cloudron /app/data

echo "==> Starting OwnTracks"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i OwnTracks
